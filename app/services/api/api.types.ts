import { QuestionSnapshot } from "../../models"
import { GeneralApiProblem } from "./api-problem"

export type GetQuestionsResult = { kind: "ok"; questions: QuestionSnapshot[] } | GeneralApiProblem
// export interface User {
//   id: number
//   name: string
// }

// export type GetUsersResult = { kind: "ok"; users: User[] } | GeneralApiProblem
// export type GetUserResult = { kind: "ok"; user: User } | GeneralApiProblem
